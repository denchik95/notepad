﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Notebook
{
    class User
    {
        private static int LastId = 0;
        public int ID { get; } = LastId++;

        // Обязательное
        public string FirstName { get; set; }
        // Обязательное
        public string LastName { get; set; }
        // Обязательное
        public string Number { get; set; }
        // Обязательное
        public string Country { get; set; }
        // Не обязательное
        public string FatherName { get; set; }
        // Не обязательное
        public string Birthday { get; set; }
        // Не обязательное
        public string Organization { get; set; }
        // Не обязательное
        public string Position { get; set; }
        // Не обязательное
        public string Other { get; set; }

    }
}
