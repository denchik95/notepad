﻿﻿using System;
using System.Collections.Generic;

namespace Notebook
{
    class AddressBook
    {
        static List<User> allUser = new List<User>();
        static void Main(string[] args)
        {
            while (true)
            {
                StartMenu();

                ConsoleKeyInfo keyInfo = Console.ReadKey();
                Console.Clear();
                switch (keyInfo.Key)
                {
                    case ConsoleKey.C:
                        CreateNewUser();
                        break;
                    case ConsoleKey.D:
                        DeleteUser();
                        break;
                    case ConsoleKey.E:
                        EditUser();
                        break;
                    case ConsoleKey.R:
                        ReadUser();
                        break;
                    case ConsoleKey.S:
                        ShawAll();
                        break;
                    case ConsoleKey.Q:
                        Environment.Exit(0);
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("Неверная команда!");
                        break;
                }

            }
        }
        static void CreateNewUser()
        {
            Console.Clear();
            User newUser = new User();
            allUser.Add(newUser);
            newUser.LastName = GetPropertyFromConsole("фамилию", true);
            newUser.FirstName = GetPropertyFromConsole("имя", true);
            newUser.FatherName = GetPropertyFromConsole("отчество", false);
            newUser.Number = GetPropertyFromConsole("номер", true, true);
            newUser.Country = GetPropertyFromConsole("страну", true);
            newUser.Birthday = GetPropertyFromConsole("дату рождения", false);
            newUser.Organization = GetPropertyFromConsole("организацию", false);
            newUser.Position = GetPropertyFromConsole("должность", false);
            newUser.Other = GetPropertyFromConsole("другие заметки", false);
            Console.WriteLine("Пользователь создан! Нажмите любую клавишу для продолжения");
            Console.ReadKey();
            Console.Clear();
        }

        static void ShawAll()
        {
            foreach (User user in allUser)
            {
                Console.WriteLine($"ID: {user.ID}");
                Console.WriteLine($"Имя {user.FirstName}");
                Console.WriteLine($"Фамилия: {user.LastName}");
                Console.WriteLine($"Номер: {user.Number}");
            }
            Console.WriteLine("Нажмите любую клавижу для продолжения");
            Console.ReadKey();
        }

        static void ReadUser()
        {
            User user = GetUserFromConsole();
            if (user == null) return;

            Console.WriteLine($"ID: {user.ID}");
            Console.WriteLine($"Фамилия: {user.LastName}");
            Console.WriteLine($"Имя: {user.FirstName}");
            Console.WriteLine($"Отчество: {user.FatherName}");
            Console.WriteLine($"Номер: {user.Number}");
            Console.WriteLine($"Страна: {user.Country}");
            Console.WriteLine($"Дата рождения: {user.Birthday}");
            Console.WriteLine($"Организация: {user.Organization}");
            Console.WriteLine($"Должность: {user.Position}");
            Console.WriteLine($"Прочие заметки: {user.Other}");
            Console.WriteLine("Нажмите любую клавижу для продолжения");
            Console.ReadKey();
        }

        static void DeleteUser()
        {
            User user = GetUserFromConsole();
            if (user == null) return;

            allUser.Remove(user);
            Console.WriteLine("Пользователь удален! Нажмите любую клавишу для продолжения");
            Console.ReadKey();

        }
        static void EditUser()
        {
            User user = GetUserFromConsole();
            if (user == null) return;

            Console.WriteLine("Измените желаемые поля (Enter - пропуск)");

            string lastName = GetPropertyFromConsole("фамилию", false);
            if (lastName != "") user.LastName = lastName;
            string firstName = GetPropertyFromConsole("имя", false);
            if (firstName != "") user.FirstName = firstName;
            string fatherName = GetPropertyFromConsole("отчество", false);
            if (fatherName != "") user.FatherName = fatherName;
            string number = GetPropertyFromConsole("номер", false, true);
            if (number != "") user.Number = number;
            string country = GetPropertyFromConsole("страну", false);
            if (country != "") user.Country = country;
            string Birthday = GetPropertyFromConsole("дату рождения", false);
            if (Birthday != "") user.Birthday = Birthday;
            string organization = GetPropertyFromConsole("организацию", false);
            if (organization != "") user.Organization = organization;
            string position = GetPropertyFromConsole("должность", false);
            if (position != "") user.Position = position;
            string other = GetPropertyFromConsole("другие заметки", false);
            if (other != "") user.Other = other;
            Console.WriteLine("Пользователь изменен! Нажмите любую клавишу для продолжения");
            Console.ReadKey();
        }

        static User GetUserFromConsole()
        {
            while (true)
            {
                try
                {
                    Console.WriteLine("Введите ID (-1 - выход в меню)");
                    int userId = int.Parse(Console.ReadLine());

                    if (userId == -1) return null;

                    foreach (User user in allUser)
                    {
                        if (user.ID == userId)
                            return user;
                    }

                    Console.WriteLine("Неверный ID");
                }
                catch (Exception x)
                {
                    Console.WriteLine("Неверный ID");
                }
            }
        }

        static string GetPropertyFromConsole(string propertyName, bool isRequired, bool onlyNumbers = false)
        {
            string options = isRequired ? " (обязательное поле)" : "";
            string message = $"Введите {propertyName}{options}:";

            string property;
            do
            {
                Console.WriteLine(message);
                property = Console.ReadLine();
            } while (property == "" && isRequired ||
                     property != "" && onlyNumbers && !long.TryParse(property, out long n));

            return property;
        }
        static void StartMenu()
        {
            Console.Clear();
            Console.WriteLine("Какую комманду вы хотите выполнить?");
            Console.WriteLine("Для добавления записи нажмите \"C\"");
            Console.WriteLine("Для удаления записи нажмите \"D\"");
            Console.WriteLine("Для редактирования записи нажмите \"E\"");
            Console.WriteLine("Для просмотра одной записи нажмите \"R\"");
            Console.WriteLine("Для просмотра всех записей нажмите \"S\"");
            Console.WriteLine("Для выхода нажмите \"Q\"");
        }
    }
}
